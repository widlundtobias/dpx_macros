extern crate proc_macro;
use proc_macro::Spacing;
use proc_macro::TokenStream;
use proc_macro::TokenTree;
//call syntax:
//{j= J, t1ex : n1, t2ex : n2, t3ex : n3}
//
// j: join entry instance name
// J: Join entry type name
//
// t#ex: an expression resolving to a table. can be & or &mut or value
// n#: name of what members to bind the result iterator value to.

#[proc_macro]
pub fn join(input: TokenStream) -> TokenStream {
    let mut input = input.into_iter();
    let join_var_name = input.next().unwrap().to_string();
    input.next();
    let join_var_type = input.next().unwrap().to_string();
    input.next();

    let input = input.collect::<Vec<TokenTree>>();

    let input = input.split(|tree| match tree {
        TokenTree::Punct(p) if p.as_char() == ',' => true,
        _ => false,
    });

    struct TableBinding {
        identifier: TokenTree,
        expressions: Vec<TokenTree>,
    }

    let mut table_bindings = Vec::<TableBinding>::new();

    fn code_from_bindings(
        v: &Vec<TableBinding>,
        string_creator: impl Fn(usize, &TableBinding) -> String,
        separator: &str,
    ) -> String {
        v.iter()
            .enumerate()
            .map(|binding_enumeration| {
                let (i, e) = binding_enumeration;
                string_creator(i, e)
            })
            .collect::<Vec<String>>()
            .join(separator)
    }

    for table_binding_tokens in input {
        let bound_ident: &TokenTree = table_binding_tokens.last().unwrap();
        let bound_expr = &table_binding_tokens[..table_binding_tokens.len() - 2];
        table_bindings.push(TableBinding {
            identifier: bound_ident.clone(),
            expressions: bound_expr.to_vec(),
        });
    }

    let code = format!(
        "
    pub struct {join_var_type}Iter<
    Id: dpx_db::table::TableId,
    {gen_type_list},
    {gen_iter_type_list_impl_iterator_entry}> {{
        {join_iter_members},
    }}
    
    impl<
    Id: dpx_db::table::TableId,
    {gen_type_list},
    {gen_iter_type_list_impl_iterator_entry}
    > {join_var_type}Iter<Id, {gen_type_list}, {gen_iter_type_list}> {{
        pub fn new(
            {new_iter_function_params}
        ) -> Self {{
            {join_var_type}Iter {{
                {join_iter_init_members},
            }}
        }}
    }}

    pub struct {join_var_type}<Id: dpx_db::table::TableId, {gen_type_list}> {{
        pub id: Id,
        {join_entry_members},
    }}
    
    impl<Id: dpx_db::table::TableId, {gen_type_list},
    {gen_iter_type_list_impl_iterator_entry}
    > Iterator for {join_var_type}Iter<Id, {gen_type_list}, {gen_iter_type_list}> {{
        type Item = {join_var_type}<Id, {gen_type_list}>;
        fn next(&mut self) -> Option<Self::Item> {{
            loop {{
                //find target ID
                let target_id = *[
                    {target_id_matchers}
                ]
                .iter()
                .max()
                .unwrap();
    
                //iterate tables until target id found
                {to_target_iterations}

                break Some({join_var_type} {{
                    id: target_id,
                    {result_entry_inits}
                }});
            }}
        }}
    }}
    
    {ordered_guards}

    let {join_var_name} = {join_var_type}Iter::new(
        {join_iter_init_expressions}
    );
    ",
        join_var_type = join_var_type,
        join_var_name = join_var_name,
        gen_type_list = code_from_bindings(&table_bindings, |i, _| format!("T{}", i), ", "),
        gen_iter_type_list = code_from_bindings(&table_bindings, |i, _| format!("TI{}", i), ", "),
        gen_iter_type_list_impl_iterator_entry = code_from_bindings(
            &table_bindings,
            |i, _| format!(
                "TI{i}: Iterator<Item = dpx_db::table::TableEntry<Id, T{i}>>",
                i = i
            ),
            ", "
        ),
        join_iter_members = code_from_bindings(
            &table_bindings,
            |i, binding| format!(
                "pub {}_iter: std::iter::Peekable<TI{}>",
                binding.identifier.to_string(),
                i
            ),
            ",\n"
        ),
        join_entry_members = code_from_bindings(
            &table_bindings,
            |i, binding| format!("pub {}: T{}", binding.identifier.to_string(), i),
            ",\n"
        ),
        new_iter_function_params = code_from_bindings(
            &table_bindings,
            |i, binding| format!("{}: TI{}", binding.identifier.to_string(), i),
            ", "
        ),
        join_iter_init_members = code_from_bindings(
            &table_bindings,
            |_, binding| format!(
                "{ident}_iter: {ident}.peekable()",
                ident = binding.identifier.to_string()
            ),
            ", "
        ),
        target_id_matchers = code_from_bindings(
            &table_bindings,
            |_, binding| format!(
                "
                match self.{ident}_iter.peek() {{
                    Some(entry) => entry.id,
                    None => return None,
                }}",
                ident = binding.identifier.to_string()
            ),
            ", "
        ),
        to_target_iterations = code_from_bindings(
            &table_bindings,
            |_, binding| format!(
                "
                let mut peeked_id;

                while {{
                    //do
                    peeked_id = match self.{ident}_iter.peek() {{
                        Some(e) => e.id,
                        None => return None,
                    }};

                    let unsatisfied = target_id > peeked_id;

                    if unsatisfied {{
                        self.{ident}_iter.next();
                    }}

                    //while
                    unsatisfied
                }} {{}}

                if peeked_id != target_id {{
                    continue;
                }}

                let good_{ident}_entry = self.{ident}_iter.next().unwrap();
            ",
                ident = binding.identifier.to_string()
            ),
            "\n "
        ),
        result_entry_inits = code_from_bindings(
            &table_bindings,
            |_, binding| format!(
                "{ident}: good_{ident}_entry.data",
                ident = binding.identifier.to_string()
            ),
            ",\n "
        ),
        join_iter_init_expressions = code_from_bindings(
            &table_bindings,
            |_, binding| format!(
                "({}).into_iter()",
                binding
                    .expressions
                    .iter()
                    .map(|e| {
                        let mut res = e.to_string();
                        if let TokenTree::Punct(p) = e {
                            if p.spacing() == Spacing::Alone {
                                res.push(' ');
                            }
                        } else {
                            res.push(' ');
                        }
                        res
                    })
                    .collect::<String>()
            ),
            ",\n "
        ),
        ordered_guards = code_from_bindings(
            &table_bindings,
            |_, binding| format!(
                "(&{}) as &dyn dpx_db::table::Ordered;",
                binding
                    .expressions
                    .iter()
                    .map(|e| {
                        let mut res = e.to_string();
                        if let TokenTree::Punct(p) = e {
                            if p.spacing() == Spacing::Alone {
                                res.push(' ');
                            }
                        } else {
                            res.push(' ');
                        }
                        res
                    })
                    .collect::<String>()
            ),
            "\n "
        ),
    );

    //println!("{}", code);

    code.parse().unwrap()
}
